# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ding package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: ding\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-04 20:42+0200\n"
"PO-Revision-Date: 2022-04-16 18:03+0200\n"
"Last-Translator: Quentin PAGÈS\n"
"Language-Team: \n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.0.1\n"

#: askRenamePopup.js:42
msgid "Folder name"
msgstr "Nom del dorsièr"

#: askRenamePopup.js:42
msgid "File name"
msgstr "Nom de fichièr"

#: askRenamePopup.js:49 desktopManager.js:791
msgid "OK"
msgstr "D’acordi"

#: askRenamePopup.js:49
msgid "Rename"
msgstr "Renomenar"

#: dbusUtils.js:66
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr "« ${programName} » es requerit per Desktop Icons"

#: dbusUtils.js:67
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""
"Per que foncione aquesta foncionalitat dins Desktop Icons, devètz installar "
"« ${programName} » dins vòstre sistèma."

#: desktopIconsUtil.js:96
msgid "Command not found"
msgstr "Comanda pas trobada"

#: desktopManager.js:214
msgid "Nautilus File Manager not found"
msgstr "Gestionari de fichièrs Nautilus pas trobat"

#: desktopManager.js:215
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""
"Gestionari de fichièrs Nautilus es obligatòri per foncionar amb Desktop "
"Icons NG."

#: desktopManager.js:754
msgid "Clear Current Selection before New Search"
msgstr "Escafar la seleccion actuala abans una recèrca novèla"

#: desktopManager.js:793 fileItemMenu.js:369
msgid "Cancel"
msgstr "Anullada"

#: desktopManager.js:795
msgid "Find Files on Desktop"
msgstr "Recercar de fichièrs al burèu"

#: desktopManager.js:860 desktopManager.js:1483
msgid "New Folder"
msgstr "Dorsièr novèl"

#: desktopManager.js:864
msgid "New Document"
msgstr "Document novèl"

#: desktopManager.js:869
msgid "Paste"
msgstr "Pegar"

#: desktopManager.js:873
msgid "Undo"
msgstr "Anullar"

#: desktopManager.js:877
msgid "Redo"
msgstr "Refar"

#: desktopManager.js:883
msgid "Select All"
msgstr "Tot seleccionar"

#: desktopManager.js:891
msgid "Show Desktop in Files"
msgstr "Mostrar lo burèu dins Fichièrs"

#: desktopManager.js:895 fileItemMenu.js:287
msgid "Open in Terminal"
msgstr "Dobrir dins un terminal"

#: desktopManager.js:901
msgid "Change Background…"
msgstr "Modificar lo rèireplan…"

#: desktopManager.js:912
msgid "Desktop Icons Settings"
msgstr "Paramètres icònas de burèu"

#: desktopManager.js:916
msgid "Display Settings"
msgstr "Paramètres d'afichatge"

#: desktopManager.js:1541
msgid "Arrange Icons"
msgstr "Arrengar las icònas"

#: desktopManager.js:1545
msgid "Arrange By..."
msgstr "Arrengar per..."

#: desktopManager.js:1554
msgid "Keep Arranged..."
msgstr "Gardar arrengat..."

#: desktopManager.js:1558
msgid "Keep Stacked by type..."
msgstr "Gardar l’empilament per tipe..."

#: desktopManager.js:1563
msgid "Sort Home/Drives/Trash..."
msgstr "Triar Repertòri personal/disques/escobilhièr..."

#: desktopManager.js:1569
msgid "Sort by Name"
msgstr "Triar per nom"

#: desktopManager.js:1571
msgid "Sort by Name Descending"
msgstr "Triar per nom descendent"

#: desktopManager.js:1574
msgid "Sort by Modified Time"
msgstr "Triar per data de modificacion"

#: desktopManager.js:1577
msgid "Sort by Type"
msgstr "Triar per tipe"

#: desktopManager.js:1580
msgid "Sort by Size"
msgstr "Triar per talha"

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:156
msgid "Home"
msgstr "Domicili"

#: fileItem.js:275
msgid "Broken Link"
msgstr "Ligam copat"

#: fileItem.js:276
msgid "Can not open this File because it is a Broken Symlink"
msgstr ""
"Dubertura d'aqueste fichièr impossibla pr’amor qu’es un ligam simbolic copat"

#: fileItem.js:326
msgid "Broken Desktop File"
msgstr "Mostrar lo burèu dins Fichièrs"

#: fileItem.js:327
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"\t<b>Edit the file to set the correct executable Program.</b>"
msgstr ""
"Aqueste fichièr .desktop a d’errors o mena a un programa sens autorizacion. "
"Pòt pas èsser executat.\n"
"\n"
"\t<b>Modificatz lo fichièr per definir un programa executable corrèct.</b>"

#: fileItem.js:333
msgid "Invalid Permissions on Desktop File"
msgstr "Autorizacion invalida sul fichièr de burèu"

#: fileItem.js:334
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""
"Aqueste fichièr .desktop a d’autorizacions incorrèctas. Fasètz clicatz drech "
"per modificar las proprietats, puèi :\n"

#: fileItem.js:336
msgid ""
"\n"
"<b>Set Permissions, in \"Others Access\", \"Read Only\" or \"None\"</b>"
msgstr ""
"\n"
"<b>Definir las autorizacions, dins « Autres accèsses », « Lectura sola » o "
"« Cap »</b>"

#: fileItem.js:339
msgid ""
"\n"
"<b>Enable option, \"Allow Executing File as a Program\"</b>"
msgstr ""
"\n"
"<b>Activar l’opcion, « Permetre d’executar lo fichièr coma programa »</b>"

#: fileItem.js:347
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"<b>Enable \"Allow Launching\"</b>"
msgstr ""
"Aqueste fichièr .desktop es pas fisable, se pòt pas lançar. Per autorizar "
"son lançament, fasètz clic drech puèi :\n"
"\n"
"<b>Activar « Autorizar lo lançament »</b>"

#: fileItemMenu.js:118
msgid "Open All..."
msgstr "Dobrir tot..."

#: fileItemMenu.js:118
msgid "Open"
msgstr "Dobrir"

#: fileItemMenu.js:129
msgid "Stack This Type"
msgstr "Apilar aqueste tipe"

#: fileItemMenu.js:129
msgid "Unstack This Type"
msgstr "Desplegar aqueste tipe"

#: fileItemMenu.js:141
msgid "Scripts"
msgstr "Scripts"

#: fileItemMenu.js:147
msgid "Open All With Other Application..."
msgstr "Dobrir tot amb una autra aplicacion..."

#: fileItemMenu.js:147
msgid "Open With Other Application"
msgstr "Dobrir amb una autra aplicacion"

#: fileItemMenu.js:153
msgid "Launch using Dedicated Graphics Card"
msgstr "Aviar en utilizant la carta grafica dedicada"

#: fileItemMenu.js:162
msgid "Run as a program"
msgstr "Executar coma un programa"

#: fileItemMenu.js:170
msgid "Cut"
msgstr "Copar"

#: fileItemMenu.js:175
msgid "Copy"
msgstr "Copiar"

#: fileItemMenu.js:181
msgid "Rename…"
msgstr "Renomenar…"

#: fileItemMenu.js:189
msgid "Move to Trash"
msgstr "Desplaçar dins l'escobilhièr"

#: fileItemMenu.js:195
msgid "Delete permanently"
msgstr "Suprimir definitivament"

#: fileItemMenu.js:203
msgid "Don't Allow Launching"
msgstr "Permetre pas l’aviada"

#: fileItemMenu.js:203
msgid "Allow Launching"
msgstr "Permetre l'aviada"

#: fileItemMenu.js:214
msgid "Empty Trash"
msgstr "Voidar l'escobilièr"

#: fileItemMenu.js:225
msgid "Eject"
msgstr "Ejectar"

#: fileItemMenu.js:231
msgid "Unmount"
msgstr "Desmontar"

#: fileItemMenu.js:241
msgid "Extract Here"
msgstr "Traire aicí"

#: fileItemMenu.js:245
msgid "Extract To..."
msgstr "Extraire dins..."

#: fileItemMenu.js:252
msgid "Send to..."
msgstr "Mandar a..."

#: fileItemMenu.js:258
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "Compressar {0} fichièr"
msgstr[1] "Compressar {0} fichièrs"

#: fileItemMenu.js:264
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "Dorsièr novèl amb {0} element"
msgstr[1] "Dorsièr novèl amb {0} elements"

#: fileItemMenu.js:273
msgid "Common Properties"
msgstr "Proprietats comunas"

#: fileItemMenu.js:273
msgid "Properties"
msgstr "Proprietats"

#: fileItemMenu.js:280
msgid "Show All in Files"
msgstr "Tot mostrar dins Fichièrs"

#: fileItemMenu.js:280
msgid "Show in Files"
msgstr "Mostrar dins Fichièrs"

#: fileItemMenu.js:365
msgid "Select Extract Destination"
msgstr "Seleccionar una destinacion per l'extraccion"

#: fileItemMenu.js:370
msgid "Select"
msgstr "Causir"

#: fileItemMenu.js:396
msgid "Can not email a Directory"
msgstr "Impossible d’enviar un email a un Repertòri"

#: fileItemMenu.js:397
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr ""
"La seleccion inclutz un repertòri, compressatz lo repertòri d'en primièr."

#: preferences.js:67
msgid "Settings"
msgstr "Paramètres"

#: prefswindow.js:46
msgid "Size for the desktop icons"
msgstr "Talha de las icònas burèu"

#: prefswindow.js:46
msgid "Tiny"
msgstr "Minuscula"

#: prefswindow.js:46
msgid "Small"
msgstr "Pichona"

#: prefswindow.js:46
msgid "Standard"
msgstr "Estandarda"

#: prefswindow.js:46
msgid "Large"
msgstr "Granda"

#: prefswindow.js:47
msgid "Show the personal folder in the desktop"
msgstr "Mostrar lo dorsièr personal pel burèu"

#: prefswindow.js:48
msgid "Show the trash icon in the desktop"
msgstr "Mostrar l'icòna de l'escobilhièr pel burèu"

#: prefswindow.js:49 schemas/org.gnome.shell.extensions.ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr "Mostrar los lector extèrns montats pel burèu"

#: prefswindow.js:50 schemas/org.gnome.shell.extensions.ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr "Mostrar los lectors ret montats pel burèu"

#: prefswindow.js:53
msgid "New icons alignment"
msgstr "Alinhament de las icònas novèlas"

#: prefswindow.js:54
msgid "Top-left corner"
msgstr "Caire en naut a esquèrra"

#: prefswindow.js:55
msgid "Top-right corner"
msgstr "Caire en naut a drecha"

#: prefswindow.js:56
msgid "Bottom-left corner"
msgstr "Caire en bas a esquèrra"

#: prefswindow.js:57
msgid "Bottom-right corner"
msgstr "Caire en bas a drecha"

#: prefswindow.js:59 schemas/org.gnome.shell.extensions.ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr "Apondre los lectors novèls al costat opausat de l’ecran"

#: prefswindow.js:60
msgid "Highlight the drop place during Drag'n'Drop"
msgstr "Enlusir la zòna de depaus pendent un lisar-depausar"

#: prefswindow.js:61 schemas/org.gnome.shell.extensions.ding.gschema.xml:90
msgid "Use Nemo to open folders"
msgstr "Utilizar Nemo per dobrir los dossièrs"

#: prefswindow.js:63
msgid "Add an emblem to soft links"
msgstr "Apondre una emblèma als ligams leugièrs"

#: prefswindow.js:65
msgid "Use dark text in icon labels"
msgstr "Utilizar un tèxte escur pels labèls d’icòna"

#. Nautilus options
#: prefswindow.js:71
msgid "Settings shared with Nautilus"
msgstr "Paramètres partejats amb Nautilus"

#: prefswindow.js:90
msgid "Click type for open files"
msgstr "Tipe de clic utilizat per dobrir de fichièrs"

#: prefswindow.js:90
msgid "Single click"
msgstr "Clic simple"

#: prefswindow.js:90
msgid "Double click"
msgstr "Clic doble"

#: prefswindow.js:91
msgid "Show hidden files"
msgstr "Visualizar los fichièrs amagats"

#: prefswindow.js:92
msgid "Show a context menu item to delete permanently"
msgstr "Mostrar un element de menú contextual per suprimir definitivament"

#: prefswindow.js:97
msgid "Action to do when launching a program from the desktop"
msgstr "Accion de realizar pendent l’aviada d’un programa a partir del burèu"

#: prefswindow.js:98
msgid "Display the content of the file"
msgstr "Afichar lo contengut del fichièr"

#: prefswindow.js:99
msgid "Launch the file"
msgstr "Executar lo fichièr"

#: prefswindow.js:100
msgid "Ask what to do"
msgstr "Demandar de qué far"

#: prefswindow.js:106
msgid "Show image thumbnails"
msgstr "Mostrar las miniaturas d'imatge"

#: prefswindow.js:107
msgid "Never"
msgstr "Jamai"

#: prefswindow.js:108
msgid "Local files only"
msgstr "Unicament los fichièrs locals"

#: prefswindow.js:109
msgid "Always"
msgstr "Totjorn"

#: showErrorPopup.js:40
msgid "Close"
msgstr "Tancar"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:25
msgid "Icon size"
msgstr "Talha de l'icòna"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr "Definir la talha de las icònas burèu."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:30
msgid "Show personal folder"
msgstr "Mostrar lo repertòri personal"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr "Mostrar lo dorsièr personal pel burèu."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:35
msgid "Show trash icon"
msgstr "Mostrar l'icòna de l'escobilhièr"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr "Mostrar l'icòna de l'escobilhièr pel burèu."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:40
msgid "New icons start corner"
msgstr "Caire per las icònas novèlas"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr "Definir lo caire ont seràn plaças las icònas en primièr."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr "Mostrar los lectors connectats a l'ordenador."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:51
msgid "Show mounted network volumes in the desktop."
msgstr "Mostrar los volums montats ret pel burèu."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""
"Pendent l’apondon de lectors e volums pel burèu, los plaçar al costat "
"opausat de l’ecran."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr "Fa veire un rectangle al lòc de destinacion pendent un lisar-depausar"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""
"Pendent una operacion de lisar-depausar, marca lo lòc per la grasilha ont "
"l’icòna serà plaçada amb un rectangle semi-transparent."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr "Triar los dorsièr especial - Repertòri personal/Escobilhièr."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""
"En arrengar las icònas del burèu, per triar e cambiar la posicion del "
"repertòri personal, l’escobilhièr, los volums montats e ret"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr "Gardar las icònas arrengadas"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr "Totjorn gardar las icònas arrengadas pel darrièr òrdre causit"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:75
msgid "Arrange Order"
msgstr "Òrdre d’arrengament"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr "Icònas arrengadas per aquesta proprietat"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:80
msgid "Keep Icons Stacked"
msgstr "Gardar las icònas arrengadas"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:81
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr "Totjorn gardar las icònas arrengadas pel darrièr òrdre causit"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr "Tipe de fichièrs d’apilar pas"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr ""
"Un tablèu amb de cadenas de tèxte de tipes, apilar pas aquestes tipes de "
"fichièrs"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:91
msgid "Use Nemo instead of Nautilus to open folders."
msgstr "Utilizar Nemo allòc de Nautilus per dobrir los dossièrs."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:95
msgid "Add an emblem to links"
msgstr "Apondre una emblèma als ligams"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:96
msgid "Add an emblem to allow to identify soft links."
msgstr ""
"Apondre una emblèma per vos permetre d’identificar los ligams leugièrs."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:100
msgid "Use black for label text"
msgstr "Utilizar lo negre pel tèxt de labèl"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:101
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""
"Emplega lo negre pels tèxtes de labèl a la plaça del blanc. Util pendent "
"l’utilizacion de fonzes clars."

#~ msgid ""
#~ "To configure Desktop Icons NG, do right-click in the desktop and choose "
#~ "the last item: 'Desktop Icons settings'"
#~ msgstr ""
#~ "Per configurar Desktop Icons NG, clicatz a drech pel burèu e causissètz  "
#~ "« Paramètres icònas de burèu »"

#~ msgid "Do you want to run “{0}”, or display its contents?"
#~ msgstr "Volètz executar « 0 » o afichar son contengut ?"

#~ msgid "“{0}” is an executable text file."
#~ msgstr "« {0} » es un fichièr tèxte executable."

#~ msgid "Execute in a terminal"
#~ msgstr "Executar dins un terminal"

#~ msgid "Show"
#~ msgstr "Visualizar"

#~ msgid "Execute"
#~ msgstr "Executar"

#~ msgid "New folder"
#~ msgstr "Repertòri novèl"
