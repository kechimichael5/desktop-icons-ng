# Slovak translation for Desktop Icons NG
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Dušan Kazik <prescott66@gmail.com>, 2020-2021.
#
msgid ""
msgstr ""
"Project-Id-Version: unnamed project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-04 20:42+0200\n"
"PO-Revision-Date: 2021-10-25 07:57+0200\n"
"Last-Translator: Dušan Kazik <prescott66@gmail.com>\n"
"Language-Team: Slovak <gnome-sk-list@gnome.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 40.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: .\n"

#: askRenamePopup.js:42
msgid "Folder name"
msgstr "Názov priečinku"

#: askRenamePopup.js:42
msgid "File name"
msgstr "Názov súboru"

#: askRenamePopup.js:49 desktopManager.js:791
msgid "OK"
msgstr "OK"

#: askRenamePopup.js:49
msgid "Rename"
msgstr "Premenovať"

#: dbusUtils.js:66
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr ""

#: dbusUtils.js:67
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""

#: desktopIconsUtil.js:96
msgid "Command not found"
msgstr "Príkaz sa nenašiel"

#: desktopManager.js:214
msgid "Nautilus File Manager not found"
msgstr "Správca súborov Nautilus sa nenašiel"

#: desktopManager.js:215
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""
"Správca súborov Nautilus je potrebný na spoluprácu s rozšírením Desktop "
"Icons NG."

#: desktopManager.js:754
msgid "Clear Current Selection before New Search"
msgstr "Pred novým hľadaním vymažte aktuálny výber"

#: desktopManager.js:793 fileItemMenu.js:369
msgid "Cancel"
msgstr "Zrušiť"

#: desktopManager.js:795
msgid "Find Files on Desktop"
msgstr "Nájdenie súborov na pracovnej ploche"

#: desktopManager.js:860 desktopManager.js:1483
msgid "New Folder"
msgstr "Nový priečinok"

#: desktopManager.js:864
msgid "New Document"
msgstr "Nový dokument"

#: desktopManager.js:869
msgid "Paste"
msgstr "Vložiť"

#: desktopManager.js:873
msgid "Undo"
msgstr "Vrátiť späť"

#: desktopManager.js:877
msgid "Redo"
msgstr "Zopakovať"

#: desktopManager.js:883
msgid "Select All"
msgstr "Vybrať všetko"

#: desktopManager.js:891
msgid "Show Desktop in Files"
msgstr "Zobraziť Plochu v aplikácii Súbory"

#: desktopManager.js:895 fileItemMenu.js:287
msgid "Open in Terminal"
msgstr "Otvoriť v termináli"

#: desktopManager.js:901
msgid "Change Background…"
msgstr "Zmeniť pozadie…"

#: desktopManager.js:912
msgid "Desktop Icons Settings"
msgstr "Nastavenia ikon pracovnej plochy"

#: desktopManager.js:916
msgid "Display Settings"
msgstr "Nastavenia displejov"

#: desktopManager.js:1541
msgid "Arrange Icons"
msgstr "Usporiadať ikony"

#: desktopManager.js:1545
msgid "Arrange By..."
msgstr "Usporiadať podľa..."

#: desktopManager.js:1554
msgid "Keep Arranged..."
msgstr "Udržiavať usporiadané..."

#: desktopManager.js:1558
msgid "Keep Stacked by type..."
msgstr ""

#: desktopManager.js:1563
msgid "Sort Home/Drives/Trash..."
msgstr "Usporiadať Domov/jednotky/Kôš..."

#: desktopManager.js:1569
msgid "Sort by Name"
msgstr "Usporiadať podľa názvu"

#: desktopManager.js:1571
msgid "Sort by Name Descending"
msgstr "Usporiadať podľa názvu zostupne"

#: desktopManager.js:1574
msgid "Sort by Modified Time"
msgstr "Usporiadať podľa času úpravy"

#: desktopManager.js:1577
msgid "Sort by Type"
msgstr "Usporiadať podľa typu"

#: desktopManager.js:1580
msgid "Sort by Size"
msgstr "Usporiadať podľa veľkosti"

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:156
msgid "Home"
msgstr "Domov"

#: fileItem.js:275
msgid "Broken Link"
msgstr ""

#: fileItem.js:276
msgid "Can not open this File because it is a Broken Symlink"
msgstr ""

#: fileItem.js:326
#, fuzzy
msgid "Broken Desktop File"
msgstr "Zobraziť Plochu v aplikácii Súbory"

#: fileItem.js:327
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"\t<b>Edit the file to set the correct executable Program.</b>"
msgstr ""

#: fileItem.js:333
msgid "Invalid Permissions on Desktop File"
msgstr ""

#: fileItem.js:334
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""

#: fileItem.js:336
msgid ""
"\n"
"<b>Set Permissions, in \"Others Access\", \"Read Only\" or \"None\"</b>"
msgstr ""

#: fileItem.js:339
msgid ""
"\n"
"<b>Enable option, \"Allow Executing File as a Program\"</b>"
msgstr ""

#: fileItem.js:347
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"<b>Enable \"Allow Launching\"</b>"
msgstr ""

#: fileItemMenu.js:118
msgid "Open All..."
msgstr "Otvoriť všetko..."

#: fileItemMenu.js:118
msgid "Open"
msgstr "Otvoriť"

#: fileItemMenu.js:129
msgid "Stack This Type"
msgstr ""

#: fileItemMenu.js:129
msgid "Unstack This Type"
msgstr ""

#: fileItemMenu.js:141
msgid "Scripts"
msgstr "Skripty"

#: fileItemMenu.js:147
msgid "Open All With Other Application..."
msgstr "Otvoriť všetko inou aplikáciou..."

#: fileItemMenu.js:147
msgid "Open With Other Application"
msgstr "Otvoriť inou aplikáciou"

#: fileItemMenu.js:153
msgid "Launch using Dedicated Graphics Card"
msgstr "Spustiť pomocou vyhradenej grafickej karty"

#: fileItemMenu.js:162
msgid "Run as a program"
msgstr ""

#: fileItemMenu.js:170
msgid "Cut"
msgstr "Vystrihnúť"

#: fileItemMenu.js:175
msgid "Copy"
msgstr "Kopírovať"

#: fileItemMenu.js:181
msgid "Rename…"
msgstr "Premenovať…"

#: fileItemMenu.js:189
msgid "Move to Trash"
msgstr "Presunúť do Koša"

#: fileItemMenu.js:195
msgid "Delete permanently"
msgstr "Odstrániť natrvalo"

#: fileItemMenu.js:203
msgid "Don't Allow Launching"
msgstr "Neumožniť spustenie"

#: fileItemMenu.js:203
msgid "Allow Launching"
msgstr "Umožniť spúšťanie"

#: fileItemMenu.js:214
msgid "Empty Trash"
msgstr "Vyprázdniť Kôš"

#: fileItemMenu.js:225
msgid "Eject"
msgstr "Vysunúť"

#: fileItemMenu.js:231
msgid "Unmount"
msgstr "Odpojiť"

#: fileItemMenu.js:241
msgid "Extract Here"
msgstr "Rozbaliť sem"

#: fileItemMenu.js:245
msgid "Extract To..."
msgstr "Rozbaliť do…"

#: fileItemMenu.js:252
msgid "Send to..."
msgstr "Odoslať do…"

#: fileItemMenu.js:258
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "Skomprimovať {0} súborov"
msgstr[1] "Skomprimovať {0} súbor"
msgstr[2] "Skomprimovať {0} súbory"

#: fileItemMenu.js:264
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "Nový priečinok s {0} položkami"
msgstr[1] "Nový priečinok s {0} položkou"
msgstr[2] "Nový priečinok s {0} položkami"

#: fileItemMenu.js:273
msgid "Common Properties"
msgstr "Spoločné vlastnosti"

#: fileItemMenu.js:273
msgid "Properties"
msgstr "Vlastnosti"

#: fileItemMenu.js:280
msgid "Show All in Files"
msgstr "Zobraziť všetko v aplikácii Súbory"

#: fileItemMenu.js:280
msgid "Show in Files"
msgstr "Zobraziť v aplikácii Súbory"

#: fileItemMenu.js:365
msgid "Select Extract Destination"
msgstr "Výber cieľa rozbaľovania"

#: fileItemMenu.js:370
msgid "Select"
msgstr "Vybrať"

#: fileItemMenu.js:396
msgid "Can not email a Directory"
msgstr "Nedá sa odoslať adresár pomocou emailu"

#: fileItemMenu.js:397
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr "Výber obsahuje adresár. Najskôr skomprimujte adresár do súboru."

#: preferences.js:67
msgid "Settings"
msgstr "Nastavenia"

#: prefswindow.js:46
msgid "Size for the desktop icons"
msgstr "Veľkosť ikon pracovnej plochy"

#: prefswindow.js:46
msgid "Tiny"
msgstr "Najmenšie"

#: prefswindow.js:46
msgid "Small"
msgstr "Malé"

#: prefswindow.js:46
msgid "Standard"
msgstr "Štandardné"

#: prefswindow.js:46
msgid "Large"
msgstr "Veľké"

#: prefswindow.js:47
msgid "Show the personal folder in the desktop"
msgstr "Zobraziť osobný priečinok na pracovnej ploche"

#: prefswindow.js:48
msgid "Show the trash icon in the desktop"
msgstr "Zobraziť ikonu Koša na pracovnej ploche"

#: prefswindow.js:49 schemas/org.gnome.shell.extensions.ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr "Zobraziť externé jednotky na pracovnej ploche"

#: prefswindow.js:50 schemas/org.gnome.shell.extensions.ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr "Zobraziť sieťové jednotky na pracovnej ploche"

#: prefswindow.js:53
msgid "New icons alignment"
msgstr "Zarovnanie nových ikon"

#: prefswindow.js:54
msgid "Top-left corner"
msgstr "Ľavý horný roh"

#: prefswindow.js:55
msgid "Top-right corner"
msgstr "Pravý horný roh"

#: prefswindow.js:56
msgid "Bottom-left corner"
msgstr "Ľavý spodný roh"

#: prefswindow.js:57
msgid "Bottom-right corner"
msgstr "Pravý spodný roh"

#: prefswindow.js:59 schemas/org.gnome.shell.extensions.ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr "Pridať nové jednotky na opačnú stranu obrazovky"

#: prefswindow.js:60
msgid "Highlight the drop place during Drag'n'Drop"
msgstr "Zvýrazniť miesto pustenia počas operácie ťahania a pustenia"

#: prefswindow.js:61 schemas/org.gnome.shell.extensions.ding.gschema.xml:90
msgid "Use Nemo to open folders"
msgstr ""

#: prefswindow.js:63
msgid "Add an emblem to soft links"
msgstr ""

#: prefswindow.js:65
msgid "Use dark text in icon labels"
msgstr ""

#. Nautilus options
#: prefswindow.js:71
msgid "Settings shared with Nautilus"
msgstr "Nastavenia zdieľané s aplikáciou Nautilus"

#: prefswindow.js:90
msgid "Click type for open files"
msgstr "Typ kliknutia na otváranie súborov"

#: prefswindow.js:90
msgid "Single click"
msgstr "Jednoduché kliknutie"

#: prefswindow.js:90
msgid "Double click"
msgstr "Dvojité kliknutie"

#: prefswindow.js:91
msgid "Show hidden files"
msgstr "Zobraziť skryté súbory"

#: prefswindow.js:92
msgid "Show a context menu item to delete permanently"
msgstr "Zobraziť kontextovú ponuku položky na trvalé odstránenie"

#: prefswindow.js:97
msgid "Action to do when launching a program from the desktop"
msgstr "Akcia, ktorú vykonať po spustení programu z pracovnej plochy"

#: prefswindow.js:98
msgid "Display the content of the file"
msgstr "Zobraziť obsah súboru"

#: prefswindow.js:99
msgid "Launch the file"
msgstr "Spustiť súbor"

#: prefswindow.js:100
msgid "Ask what to do"
msgstr "Spýtať sa, čo robiť"

#: prefswindow.js:106
msgid "Show image thumbnails"
msgstr "Zobraziť miniatúry obrázkov"

#: prefswindow.js:107
msgid "Never"
msgstr "Nikdy"

#: prefswindow.js:108
msgid "Local files only"
msgstr "Iba miestne súbory"

#: prefswindow.js:109
msgid "Always"
msgstr "Vždy"

#: showErrorPopup.js:40
msgid "Close"
msgstr "Zavrieť"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:25
msgid "Icon size"
msgstr "Veľkosť ikon"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr "Nastaví veľkosť ikon na pracovnej ploche."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:30
msgid "Show personal folder"
msgstr "Zobraziť osobný priečinok"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr "Zobrazí osobný priečinok na pracovnej ploche."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:35
msgid "Show trash icon"
msgstr "Zobraziť ikonu Koša"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr "Zobrazí ikonu Koša na pracovnej ploche."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:40
msgid "New icons start corner"
msgstr "Počiatočný roh pre nové ikony"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr "Nastaví roh, z ktorého začnú byť umiestňované ikony."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr "Zobrazí diskové jednotky pripojené k počítaču."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:51
msgid "Show mounted network volumes in the desktop."
msgstr "Zobrazí pripojené sieťové zväzky na pracovnej ploche."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""
"Pri pridaní jednotiek a zväzkov na pracovnú plochu, budú pridané na opačnú "
"stranu obrazovky."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr "Zobrazí obdĺžnik v cieľovom umiestnení počas ťahania a pustenia"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""
"Počas vykonávania operácie ťahania a pustenia sa označí miesto v mriežke, "
"kde bude ikona umiestnená, polopriehladným obdĺžnikom."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr "Usporiadať špeciálne priečinky - Domov/Kôš, jednotky."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""
"Pri usporiadaní ikon na ploche, usporiadať a zmeniť pozíciu ikon pre Domov, "
"Kôš a pripojených sieťových alebo externých jednotiek"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr "Udržiavať ikony usporiadané"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr "Vždy zachovať ikony usporiadané podľa posledného poradia usporiadania"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:75
msgid "Arrange Order"
msgstr "Poradie usporiadania"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr "Ikony budú zoradené podľa tejto vlastnosti"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:80
#, fuzzy
msgid "Keep Icons Stacked"
msgstr "Udržiavať ikony usporiadané"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:81
#, fuzzy
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr "Vždy zachovať ikony usporiadané podľa posledného poradia usporiadania"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:91
msgid "Use Nemo instead of Nautilus to open folders."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:95
msgid "Add an emblem to links"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:96
msgid "Add an emblem to allow to identify soft links."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:100
msgid "Use black for label text"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:101
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""

#~ msgid ""
#~ "To configure Desktop Icons NG, do right-click in the desktop and choose "
#~ "the last item: 'Desktop Icons settings'"
#~ msgstr ""
#~ "Na nastavenie rozšírenia Desktop Icons NG kliknite pravým tlačidlom na "
#~ "pracovnú plochu a zvoľte poslednú položku „Nastavenia ikon pracovnej "
#~ "plochy“"

#~ msgid "Do you want to run “{0}”, or display its contents?"
#~ msgstr "Chcete súbor „{0}“ spustiť, alebo sa má zobraziť jeho obsah?"

#~ msgid "“{0}” is an executable text file."
#~ msgstr "„{0}“ je spustiteľným textovým súborom."

#~ msgid "Execute in a terminal"
#~ msgstr "Spustiť v termináli"

#~ msgid "Show"
#~ msgstr "Zobraziť"

#~ msgid "Execute"
#~ msgstr "Spustiť"
