# Simplified Chinese translation for desktop-icons.
# Copyright (C) 2018 desktop-icons's COPYRIGHT HOLDER
# This file is distributed under the same license as the desktop-icons package.
# Okeyja <bross049deng@gmail.com>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: desktop-icons master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-04 20:42+0200\n"
"PO-Revision-Date: 2021-08-05 01:21+0800\n"
"Last-Translator: Okeyja <phpluo@gmail.com>\n"
"Language-Team: \n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: askRenamePopup.js:42
msgid "Folder name"
msgstr "文件夹名称"

#: askRenamePopup.js:42
msgid "File name"
msgstr "文件名称"

#: askRenamePopup.js:49 desktopManager.js:791
msgid "OK"
msgstr "确定"

#: askRenamePopup.js:49
msgid "Rename"
msgstr "重命名"

#: dbusUtils.js:66
msgid "\"${programName}\" is needed for Desktop Icons"
msgstr ""

#: dbusUtils.js:67
msgid ""
"For this functionality to work in Desktop Icons, you must install "
"\"${programName}\" in your system."
msgstr ""

#: desktopIconsUtil.js:96
msgid "Command not found"
msgstr "命令无法找到"

#: desktopManager.js:214
msgid "Nautilus File Manager not found"
msgstr "Nautilus 文件管理器找不到"

#: desktopManager.js:215
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr "Nautilus 文件管理器强制与 Desktop Icons NG 运行。"

#: desktopManager.js:754
msgid "Clear Current Selection before New Search"
msgstr ""

#: desktopManager.js:793 fileItemMenu.js:369
msgid "Cancel"
msgstr "取消"

#: desktopManager.js:795
msgid "Find Files on Desktop"
msgstr ""

#: desktopManager.js:860 desktopManager.js:1483
msgid "New Folder"
msgstr "新建文件夹"

#: desktopManager.js:864
msgid "New Document"
msgstr "新建文档"

#: desktopManager.js:869
msgid "Paste"
msgstr "粘贴"

#: desktopManager.js:873
msgid "Undo"
msgstr "复原"

#: desktopManager.js:877
msgid "Redo"
msgstr "重做"

#: desktopManager.js:883
#, fuzzy
msgid "Select All"
msgstr "全选"

#: desktopManager.js:891
msgid "Show Desktop in Files"
msgstr "在文件管理器中打开桌面"

#: desktopManager.js:895 fileItemMenu.js:287
msgid "Open in Terminal"
msgstr "在终端中打开"

#: desktopManager.js:901
msgid "Change Background…"
msgstr "更改背景..."

#: desktopManager.js:912
#, fuzzy
msgid "Desktop Icons Settings"
msgstr "设置"

#: desktopManager.js:916
msgid "Display Settings"
msgstr "显示设置"

#: desktopManager.js:1541
msgid "Arrange Icons"
msgstr "图标排序"

#: desktopManager.js:1545
msgid "Arrange By..."
msgstr "排序方式..."

#: desktopManager.js:1554
msgid "Keep Arranged..."
msgstr "保持安排..."

#: desktopManager.js:1558
msgid "Keep Stacked by type..."
msgstr ""

#: desktopManager.js:1563
msgid "Sort Home/Drives/Trash..."
msgstr "排序主目录/驱动器/回收站..."

#: desktopManager.js:1569
msgid "Sort by Name"
msgstr "按名称排序"

#: desktopManager.js:1571
msgid "Sort by Name Descending"
msgstr "按描述排序"

#: desktopManager.js:1574
msgid "Sort by Modified Time"
msgstr "按修改时间排序"

#: desktopManager.js:1577
msgid "Sort by Type"
msgstr "按类型排序"

#: desktopManager.js:1580
msgid "Sort by Size"
msgstr "按大小排序"

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:156
msgid "Home"
msgstr "主目录"

#: fileItem.js:275
msgid "Broken Link"
msgstr ""

#: fileItem.js:276
msgid "Can not open this File because it is a Broken Symlink"
msgstr ""

#: fileItem.js:326
#, fuzzy
msgid "Broken Desktop File"
msgstr "在文件管理器中打开桌面"

#: fileItem.js:327
msgid ""
"This .desktop file has errors or points to a program without permissions. It "
"can not be executed.\n"
"\n"
"\t<b>Edit the file to set the correct executable Program.</b>"
msgstr ""

#: fileItem.js:333
msgid "Invalid Permissions on Desktop File"
msgstr ""

#: fileItem.js:334
msgid ""
"This .desktop File has incorrect Permissions. Right Click to edit "
"Properties, then:\n"
msgstr ""

#: fileItem.js:336
msgid ""
"\n"
"<b>Set Permissions, in \"Others Access\", \"Read Only\" or \"None\"</b>"
msgstr ""

#: fileItem.js:339
msgid ""
"\n"
"<b>Enable option, \"Allow Executing File as a Program\"</b>"
msgstr ""

#: fileItem.js:347
msgid ""
"This .desktop file is not trusted, it can not be launched. To enable "
"launching, right-click, then:\n"
"\n"
"<b>Enable \"Allow Launching\"</b>"
msgstr ""

#: fileItemMenu.js:118
msgid "Open All..."
msgstr "全部打开..."

#: fileItemMenu.js:118
msgid "Open"
msgstr "打开"

#: fileItemMenu.js:129
msgid "Stack This Type"
msgstr ""

#: fileItemMenu.js:129
msgid "Unstack This Type"
msgstr ""

#: fileItemMenu.js:141
msgid "Scripts"
msgstr "脚本"

#: fileItemMenu.js:147
msgid "Open All With Other Application..."
msgstr "用其他方式打开。"

#: fileItemMenu.js:147
msgid "Open With Other Application"
msgstr "用其他方式打开"

#: fileItemMenu.js:153
msgid "Launch using Dedicated Graphics Card"
msgstr "使用独立显卡启动"

#: fileItemMenu.js:162
msgid "Run as a program"
msgstr ""

#: fileItemMenu.js:170
msgid "Cut"
msgstr "剪切"

#: fileItemMenu.js:175
msgid "Copy"
msgstr "复制"

#: fileItemMenu.js:181
msgid "Rename…"
msgstr "重命名……"

#: fileItemMenu.js:189
msgid "Move to Trash"
msgstr "放入回收站"

#: fileItemMenu.js:195
msgid "Delete permanently"
msgstr "永久删除"

#: fileItemMenu.js:203
msgid "Don't Allow Launching"
msgstr "不允许运行"

#: fileItemMenu.js:203
msgid "Allow Launching"
msgstr "允许运行"

#: fileItemMenu.js:214
msgid "Empty Trash"
msgstr "清空回收站"

#: fileItemMenu.js:225
msgid "Eject"
msgstr "驱逐"

#: fileItemMenu.js:231
msgid "Unmount"
msgstr "弹出"

#: fileItemMenu.js:241
msgid "Extract Here"
msgstr "提取到此处"

#: fileItemMenu.js:245
msgid "Extract To..."
msgstr "提取到…"

#: fileItemMenu.js:252
msgid "Send to..."
msgstr "发送到…"

#: fileItemMenu.js:258
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "压缩{0}文件"

#: fileItemMenu.js:264
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "带{0}项的新文件夹"

#: fileItemMenu.js:273
msgid "Common Properties"
msgstr "通用属性"

#: fileItemMenu.js:273
msgid "Properties"
msgstr "属性"

#: fileItemMenu.js:280
msgid "Show All in Files"
msgstr "全部在文件中展示"

#: fileItemMenu.js:280
msgid "Show in Files"
msgstr "在文件夹中显示"

#: fileItemMenu.js:365
msgid "Select Extract Destination"
msgstr "选择提取目录"

#: fileItemMenu.js:370
msgid "Select"
msgstr "选择"

#: fileItemMenu.js:396
msgid "Can not email a Directory"
msgstr "文件夹不能邮件"

#: fileItemMenu.js:397
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr "选择包括目录，首先将目录压缩到文件中。"

#: preferences.js:67
msgid "Settings"
msgstr "设置"

#: prefswindow.js:46
msgid "Size for the desktop icons"
msgstr "桌面图标大小"

#: prefswindow.js:46
msgid "Tiny"
msgstr "微"

#: prefswindow.js:46
msgid "Small"
msgstr "小"

#: prefswindow.js:46
msgid "Standard"
msgstr "中"

#: prefswindow.js:46
msgid "Large"
msgstr "大"

#: prefswindow.js:47
msgid "Show the personal folder in the desktop"
msgstr "在桌面显示个人文件夹"

#: prefswindow.js:48
msgid "Show the trash icon in the desktop"
msgstr "在桌面显示回收站图标"

#: prefswindow.js:49 schemas/org.gnome.shell.extensions.ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr "在桌面显示外置驱动器"

#: prefswindow.js:50 schemas/org.gnome.shell.extensions.ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr "在桌面显示个人文件夹"

#: prefswindow.js:53
msgid "New icons alignment"
msgstr "新图标排列"

#: prefswindow.js:54
msgid "Top-left corner"
msgstr "左上角"

#: prefswindow.js:55
msgid "Top-right corner"
msgstr "右上角"

#: prefswindow.js:56
msgid "Bottom-left corner"
msgstr "左下角"

#: prefswindow.js:57
msgid "Bottom-right corner"
msgstr "右下角"

#: prefswindow.js:59 schemas/org.gnome.shell.extensions.ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr "在屏幕对面展示新驱动器"

#: prefswindow.js:60
msgid "Highlight the drop place during Drag'n'Drop"
msgstr "拖拽图标时高亮显示放置区域"

#: prefswindow.js:61 schemas/org.gnome.shell.extensions.ding.gschema.xml:90
msgid "Use Nemo to open folders"
msgstr ""

#: prefswindow.js:63
msgid "Add an emblem to soft links"
msgstr ""

#: prefswindow.js:65
msgid "Use dark text in icon labels"
msgstr ""

#. Nautilus options
#: prefswindow.js:71
msgid "Settings shared with Nautilus"
msgstr "共享Nautilus的设置"

#: prefswindow.js:90
msgid "Click type for open files"
msgstr "打开文件的点击方式"

#: prefswindow.js:90
msgid "Single click"
msgstr "单击"

#: prefswindow.js:90
msgid "Double click"
msgstr "双击"

#: prefswindow.js:91
msgid "Show hidden files"
msgstr "显示隐藏文件"

#: prefswindow.js:92
msgid "Show a context menu item to delete permanently"
msgstr "永久删除时展示确认菜单项目"

#: prefswindow.js:97
msgid "Action to do when launching a program from the desktop"
msgstr "在桌面运行程序时执行的动作"

#: prefswindow.js:98
msgid "Display the content of the file"
msgstr "展示文件内容"

#: prefswindow.js:99
msgid "Launch the file"
msgstr "运行文件"

#: prefswindow.js:100
msgid "Ask what to do"
msgstr "询问如何做"

#: prefswindow.js:106
msgid "Show image thumbnails"
msgstr "显示图片缩略图"

#: prefswindow.js:107
msgid "Never"
msgstr "从不"

#: prefswindow.js:108
msgid "Local files only"
msgstr "仅本地文件"

#: prefswindow.js:109
msgid "Always"
msgstr "始终"

#: showErrorPopup.js:40
msgid "Close"
msgstr "关闭"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:25
msgid "Icon size"
msgstr "图标大小"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr "设置桌面图标大小。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:30
msgid "Show personal folder"
msgstr "显示个人文件夹"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr "在桌面显示个人文件夹。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:35
msgid "Show trash icon"
msgstr "显示回收站"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr "在桌面中显示回收站图标。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:40
msgid "New icons start corner"
msgstr "新图标开始角"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr "设置图标开始放置的角落。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr "显示连接到计算机的磁盘驱动器。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:51
msgid "Show mounted network volumes in the desktop."
msgstr "显示桌面中安装的网络卷。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr "将驱动器和卷添加到桌面时，将它们添加到屏幕的另一侧。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr "在拖拽（DnD）期间显示目标位置的矩形"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr "当执行拖拽时，在网格中用半透明矩形标记图标将被放置的位置。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr "排序特殊文件夹 - 主目录/回收站驱动器。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""
"在桌面上排列图标时，要对主目录、回收站和安装的网络或外部驱动器的位置进行排序"
"和更改"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr "保持图标排列"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr "始终保留由最后安排的订单安排的图标"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:75
msgid "Arrange Order"
msgstr "排列顺序"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr "按此属性排列图标"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:80
#, fuzzy
msgid "Keep Icons Stacked"
msgstr "保持图标排列"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:81
#, fuzzy
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr "始终保留由最后安排的订单安排的图标"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:91
msgid "Use Nemo instead of Nautilus to open folders."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:95
msgid "Add an emblem to links"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:96
msgid "Add an emblem to allow to identify soft links."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:100
msgid "Use black for label text"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:101
msgid ""
"Paint the label text in black instead of white. Useful when using light "
"backgrounds."
msgstr ""

#~ msgid ""
#~ "To configure Desktop Icons NG, do right-click in the desktop and choose "
#~ "the last item: 'Desktop Icons settings'"
#~ msgstr "设置 Desktop Icons NG，在桌面点击右键并选择最后一个项：设置"

#~ msgid "Do you want to run “{0}”, or display its contents?"
#~ msgstr "您是想运行\"{0}\"，还是显示其内容？"

#~ msgid "“{0}” is an executable text file."
#~ msgstr "“{0}” 是一个可执行程序。"

#~ msgid "Execute in a terminal"
#~ msgstr "打开终端"

#~ msgid "Show"
#~ msgstr "显示"

#~ msgid "Execute"
#~ msgstr "运行"

#~ msgid "New folder"
#~ msgstr "新建文件夹"

#~ msgid "Delete"
#~ msgstr "删除"

#~ msgid "Error while deleting files"
#~ msgstr "删除文件发生错误"

#~ msgid "Are you sure you want to permanently delete these items?"
#~ msgstr "您确定要永久删除这些项目吗？"

#~ msgid "If you delete an item, it will be permanently lost."
#~ msgstr "删除一个项目，它将会永远消失."

#~ msgid "Show external disk drives in the desktop"
#~ msgstr "在桌面中显示外部磁盘驱动器"

#~ msgid "Show the external drives"
#~ msgstr "在桌面展示个人驱动器"

#~ msgid "Show network volumes"
#~ msgstr "展示网络卷"

#~ msgid "Huge"
#~ msgstr "巨大"
